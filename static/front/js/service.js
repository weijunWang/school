function myFactory($window){
    var service = {
        // 本地储存单个值
        setStorage: function(key, value){
            $window.localStorage[key] = value;
        },
        // 读取本地存储单个值
        getStorage: function(key){
            return  $window.localStorage[key];
        },
        // 本地存储对象
        setObjStorage: function(key,value){
            $window.localStorage[key] = JSON.stringify(value);
        },
        // 读取本地存储对象
        getObjStorage: function(key){
            return JSON.parse($window.localStorage[key] || '{}');
        },
        // cookie
        setCookie: function(cname,cvalue,exdays){
            var d = new Date();
            d.setTime(d.getTime()+(exdays*24*60*60*1000));
            var expires = "expires="+d.toGMTString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        },

        getCookie: function(cname){
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) 
            {
                var c = ca[i].trim();
                if (c.indexOf(name)==0) return c.substring(name.length,c.length);
            }
            return "";
        },

        checkCookie: function(){
            var user=getCookie("username");
            if (user!="")
            {
                alert("Welcome again " + user);
            }else{
                user = prompt("Please enter your name:","");
                if (user!="" && user!=null)
                {
                setCookie("username",user,365);
                }
            }
        },
    }

    return service;
}

angular
    .module('inspinia')
    .factory('myFactory',myFactory)
    .factory('timestampMarker', ["$rootScope", function ($rootScope) {
        var timestampMarker = {
        request: function (config) {
        $rootScope.loading = true;
        config.requestTimestamp = new Date().getTime();
        return config;
        },
        response: function (response) {
        $rootScope.loading = false;
        response.config.responseTimestamp = new Date().getTime();
        return response;
        }
        };
        return timestampMarker;
    }]);