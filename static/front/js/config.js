/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider) {
    $httpProvider.interceptors.push('timestampMarker');
    $urlRouterProvider.otherwise("index/partyAffMana/ptAfManNotice");
    var _lazyLoad = function (loaded) {
        return function ($ocLazyLoad) {
            return $ocLazyLoad.load(loaded, {
                serie: true,
            });
        };
    };
    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "../../static/front/views/common/content.html",
            data: { pageTitle: 'Y.SYS' }
        })

        .state('index.main', {
            url: "/main",
            templateUrl: "../../static/front/views/main.html",
            data: { pageTitle: 'Example view' }
        })
        .state('index.minor', {
            url: "/minor",
            templateUrl: "../../static/front/views/minor.html",
            data: { pageTitle: 'Example view' }
        })
        .state("index.allProList", {
            url: "/customer/allProList",
            templateUrl: "../../static/front/views/customer/allProList.html"
        })
        .state('index.login', {
            url: '/login',
            templateUrl: '../../static/front/views/login.html',
            controller: 'LoginCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/login.js'),
            },
        })
        // 学生---首页
        .state("index.home", {
            url: "/home",
            templateUrl: "../../static/front/views/home.html",
            controller: 'HomeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/home.js'),
            },
        })
        .state("index.scientificResearchMana", {
            url: "/scientificResearchMana",
            templateUrl: "../../static/front/views/scientificResearchMana.html",
            controller: 'ScientificResearchManaCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/scientificResearchMana.js'),
            },
        })
        .state("index.student", {
            url: "/student",
            templateUrl: "../../static/front/views/student/index.html",
            controller: 'StudentIndexCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/index.js'),
            },
        })
        .state("index.personInfo", {
            url: "/student/personInfo",
            templateUrl: "../../static/front/views/student/personInfo.html",
            controller: 'PersonInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/personInfo.js'),
            },
        })
        .state("index.checkWork", {
            url: "/student/checkWork",
            templateUrl: "../../static/front/views/student/checkWork.html",
            controller: 'CheckWorkCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/checkWork.js'),
            },
        })
        .state("index.cancelLeave", {
            url: "/student/cancelLeave",
            templateUrl: "../../static/front/views/student/cancelLeave.html",
            controller: 'CancelLeaveCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/cancelLeave.js'),
            },
        })
        .state("index.leave", {
            url: "/student/leave",
            templateUrl: "../../static/front/views/student/leave.html",
            controller: 'LeaveCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/leave.js'),
            },
        })
        .state("index.poolInfo", {
            url: "/student/poolInfo",
            templateUrl: "../../static/front/views/student/poolInfo.html",
            controller: 'PoolInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/poolInfo.js'),
            },
        })
        .state("index.poolInfoNotice", {
            url: "/student/poolInfoNotice",
            templateUrl: "../../static/front/views/student/poolInfoNotice.html",
            controller: 'PoolInfoNoticeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/poolInfoNotice.js'),
            },
        })
        .state("index.scholarship", {
            url: "/student/scholarship",
            templateUrl: "../../static/front/views/student/scholarship.html",
            controller: 'ScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/scholarship.js'),
            },
        })
        .state("index.grant", {
            url: "/student/grant",
            templateUrl: "../../static/front/views/student/grant.html",
            controller: 'GrantCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/grant.js'),
            },
        })
        .state("index.workStudy", {
            url: "/student/workStudy",
            templateUrl: "../../static/front/views/student/workStudy.html",
            controller: 'WorkStudyCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/workStudy.js'),
            },
        })
        .state("index.threeGoodStudent", {
            url: "/student/threeGoodStudent",
            templateUrl: "../../static/front/views/student/threeGoodStudent.html",
            controller: 'ThreeGoodStudentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/threeGoodStudent.js'),
            },
        })
        .state("index.classLeader", {
            url: "/student/classLeader",
            templateUrl: "../../static/front/views/student/classLeader.html",
            controller: 'ClassLeaderCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/classLeader.js'),
            },
        })
        .state("index.otherReward", {
            url: "/student/otherReward",
            templateUrl: "../../static/front/views/student/otherReward.html",
            controller: 'OtherRewardCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/otherReward.js'),
            },
        })
        .state("index.punishment", {
            url: "/student/punishment",
            templateUrl: "../../static/front/views/student/punishment.html",
            controller: 'PunishmentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/punishment.js'),
            },
        })
        .state("index.psychology", {
            url: "/student/psychology",
            templateUrl: "../../static/front/views/student/psychology.html",
            controller: 'PsychologyCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/psychology.js'),
            },
        })
        .state("index.safe", {
            url: "/student/safe",
            templateUrl: "../../static/front/views/student/safe.html",
            controller: 'SafeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/safe.js'),
            },
        })
        .state("index.poolInfoRegister", {
            url: "/student/poolInfoRegister",
            templateUrl: "../../static/front/views/student/poolInfoRegister.html",
            controller: 'PoolInfoRegisterCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/poolInfoRegister.js'),
            },
        })
        .state("index.poolAlleviation", {
            url: "/student/poolAlleviation",
            templateUrl: "../../static/front/views/student/poolAlleviation.html",
            controller: 'PoolAlleviationCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/poolAlleviation.js'),
            },
        })
        .state("index.selfSupport", {
            url: "/student/selfSupport",
            templateUrl: "../../static/front/views/student/selfSupport.html",
            controller: 'SelfSupportCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfSupport.js'),
            },
        })
        .state("index.grantRule", {
            url: "/student/grantRule",
            templateUrl: "../../static/front/views/student/grantRule.html",
            controller: 'GrantRuleCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/grantRule.js'),
            },
        })
        .state("index.grantForm", {
            url: "/student/grantForm",
            templateUrl: "../../static/front/views/student/grantForm.html",
            controller: 'GrantFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/grantForm.js'),
            },
        })
        .state("index.homePoolForm", {
            url: "/student/homePoolForm",
            templateUrl: "../../static/front/views/student/homePoolForm.html",
            controller: 'HomePoolFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/homePoolForm.js'),
            },
        })
        .state("index.selfGrant", {
            url: "/student/selfGrant",
            templateUrl: "../../static/front/views/student/selfGrant.html",
            controller: 'SelfGrantCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfGrant.js'),
            },
        })
        .state("index.nationalScholarship", {
            url: "/student/nationalScholarship",
            templateUrl: "../../static/front/views/student/nationalScholarship.html",
            controller: 'NationalScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/nationalScholarship.js'),
            },
        })
        .state("index.selfScholarship", {
            url: "/student/selfScholarship",
            templateUrl: "../../static/front/views/student/selfScholarship.html",
            controller: 'SelfScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfScholarship.js'),
            },
        })
        .state("index.selfImprovement", {
            url: "/student/selfImprovement",
            templateUrl: "../../static/front/views/student/selfImprovement.html",
            controller: 'SelfImprovementCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfImprovement.js'),
            },
        })
        .state("index.encouragementScholarship", {
            url: "/student/encouragementScholarship",
            templateUrl: "../../static/front/views/student/encouragementScholarship.html",
            controller: 'EncouragementScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/encouragementScholarship.js'),
            },
        })
        .state("index.encouragementScholarshipForm", {
            url: "/student/encouragementScholarshipForm",
            templateUrl: "../../static/front/views/student/encouragementScholarshipForm.html",
            controller: 'EncouragementScholarshipFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/encouragementScholarshipForm.js'),
            },
        })
        .state("index.selfEncouragementScholarship", {
            url: "/student/selfEncouragementScholarship",
            templateUrl: "../../static/front/views/student/selfEncouragementScholarship.html",
            controller: 'SelfEncouragementScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfEncouragementScholarship.js'),
            },
        })
        .state("index.zhongNanShanscholarship", {
            url: "/student/zhongNanShanscholarship",
            templateUrl: "../../static/front/views/student/zhongNanShanscholarship.html",
            controller: 'ZhongNanShanscholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/zhongNanShanscholarship.js'),
            },
        })
        .state("index.selfZhongNanShan", {
            url: "/student/selfZhongNanShan",
            templateUrl: "../../static/front/views/student/selfZhongNanShan.html",
            controller: 'SelfZhongNanShanCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfZhongNanShan.js'),
            },
        })
        .state("index.guizhouScholarship", {
            url: "/student/guizhouScholarship",
            templateUrl: "../../static/front/views/student/guizhouScholarship.html",
            controller: 'GuizhouScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/guizhouScholarship.js'),
            },
        })
        .state("index.selfGuizhouScholarship", {
            url: "/student/selfGuizhouScholarship",
            templateUrl: "../../static/front/views/student/selfGuizhouScholarship.html",
            controller: 'SelfGuizhouScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfGuizhouScholarship.js'),
            },
        })
        .state("index.workStudyForm", {
            url: "/student/workStudyForm",
            templateUrl: "../../static/front/views/student/workStudyForm.html",
            controller: 'WorkStudyFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/workStudyForm.js'),
            },
        })
        .state("index.selfWorkStudy", {
            url: "/student/selfWorkStudy",
            templateUrl: "../../static/front/views/student/selfWorkStudy.html",
            controller: 'SelfWorkStudyCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfWorkStudy.js'),
            },
        })
        .state("index.threeGoodStudentForm", {
            url: "/student/threeGoodStudentForm",
            templateUrl: "../../static/front/views/student/threeGoodStudentForm.html",
            controller: 'ThreeGoodStudentFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/threeGoodStudentForm.js'),
            },
        })
        .state("index.selfThreeGoodStudent", {
            url: "/student/selfThreeGoodStudent",
            templateUrl: "../../static/front/views/student/selfThreeGoodStudent.html",
            controller: 'SelfThreeGoodStudentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfThreeGoodStudent.js'),
            },
        })
        .state("index.classLeaderForm", {
            url: "/student/classLeaderForm",
            templateUrl: "../../static/front/views/student/classLeaderForm.html",
            controller: 'ClassLeaderFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/classLeaderForm.js'),
            },
        })
        .state("index.selfClassLeader", {
            url: "/student/selfClassLeader",
            templateUrl: "../../static/front/views/student/selfClassLeader.html",
            controller: 'SelfClassLeaderCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfClassLeader.js'),
            },
        })
        .state("index.selfOtherReward", {
            url: "/student/selfOtherReward",
            templateUrl: "../../static/front/views/student/selfOtherReward.html",
            controller: 'SelfOtherRewardCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfOtherReward.js'),
            },
        })
        .state("index.selfPunishment", {
            url: "/student/selfPunishment",
            templateUrl: "../../static/front/views/student/selfPunishment.html",
            controller: 'SelfPunishmentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfPunishment.js'),
            },
        })
        .state("index.psychologyHelp", {
            url: "/student/psychologyHelp",
            templateUrl: "../../static/front/views/student/psychologyHelp.html",
            controller: 'PsychologyHelpCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/psychologyHelp.js'),
            },
        })
        .state("index.psychologyForm", {
            url: "/student/psychologyForm",
            templateUrl: "../../static/front/views/student/psychologyForm.html",
            controller: 'PsychologyFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/psychologyForm.js'),
            },
        })
        .state("index.psychologyChat", {
            url: "/student/psychologyChat",
            templateUrl: "../../static/front/views/student/psychologyChat.html",
            controller: 'PsychologyChatCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/psychologyChat.js'),
            },
        })
        .state("index.psychologyChatForm", {
            url: "/student/psychologyChatForm",
            templateUrl: "../../static/front/views/student/psychologyChatForm.html",
            controller: 'PsychologyChatFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/psychologyChatForm.js'),
            },
        })
        .state("index.selfSafe", {
            url: "/student/selfSafe",
            templateUrl: "../../static/front/views/student/selfSafe.html",
            controller: 'SelfSafeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/selfSafe.js'),
            },
        })
        .state("index.addSafeForm", {
            url: "/student/addSafeForm",
            templateUrl: "../../static/front/views/student/addSafeForm.html",
            controller: 'AddSafeFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/addSafeForm.js'),
            },
        })
        .state("index.whereabouts", {
            url: "/student/whereabouts",
            templateUrl: "../../static/front/views/student/whereabouts.html",
            controller: 'WhereaboutsCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/whereabouts.js'),
            },
        })
        .state("index.whereaboutsRegister", {
            url: "/student/whereaboutsRegister",
            templateUrl: "../../static/front/views/student/whereaboutsRegister.html",
            controller: 'WhereaboutsRegisterCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/student/whereaboutsRegister.js'),
            },
        })

        // 学生--辅导员
        .state("index.instructor", {
            url: "/instructor",
            templateUrl: "../../static/front/views/instructor/index.html",
            controller: 'InstructorCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/index.js')
            },
        })
        .state("index.classManage", {
            url: "/instructor/classManage",
            templateUrl: "../../static/front/views/instructor/classManage.html",
            controller: 'ClassManageCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/classManage.js')
            },
        })
        .state("index.fdyOneClass", {
            url: "/instructor/fdyOneClass",
            templateUrl: "../../static/front/views/instructor/fdyOneClass.html",
            controller: 'FdyOneClassCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyOneClass.js')
            },
        })
        .state("index.fdyCheckWork", {
            url: "/instructor/fdyCheckWork",
            templateUrl: "../../static/front/views/instructor/fdyCheckWork.html",
            controller: 'FdyCheckWorkCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyCheckWork.js')
            },
        })
        .state("index.fdyOneLeave", {
            url: "/instructor/fdyOneLeave",
            templateUrl: "../../static/front/views/instructor/fdyOneLeave.html",
            controller: 'FdyOneLeaveCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyOneLeave.js')
            },
        })
        .state("index.fdyCancelLeave", {
            url: "/instructor/fdyCancelLeave",
            templateUrl: "../../static/front/views/instructor/fdyCancelLeave.html",
            controller: 'FdyCancelLeaveCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyCancelLeave.js')
            },
        })
        .state("index.fdyWorkLog", {
            url: "/instructor/fdyWorkLog",
            templateUrl: "../../static/front/views/instructor/fdyWorkLog.html",
            controller: 'FdyWorkLogCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyWorkLog.js')
            },
        })
        .state("index.fdyWorkRecord", {
            url: "/instructor/fdyWorkRecord",
            templateUrl: "../../static/front/views/instructor/fdyWorkRecord.html",
            controller: 'FdyWorkRecordCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyWorkRecord.js')
            },
        })
        .state("index.fdyOneRecord", {
            url: "/instructor/fdyOneRecord",
            templateUrl: "../../static/front/views/instructor/fdyOneRecord.html",
            controller: 'FdyOneRecordCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyOneRecord.js')
            },
        })
        .state("index.fdyAddRecord", {
            url: "/instructor/fdyAddRecord",
            templateUrl: "../../static/front/views/instructor/fdyAddRecord.html",
            controller: 'FdyAddRecordCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyAddRecord.js')
            },
        })
        .state("index.fdyPoolInfo", {
            url: "/instructor/fdyPoolInfo",
            templateUrl: "../../static/front/views/instructor/fdyPoolInfo.html",
            controller: 'FdyPoolInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPoolInfo.js')
            },
        })
        .state("index.fdyPoolOneClass", {
            url: "/instructor/fdyPoolOneClass",
            templateUrl: "../../static/front/views/instructor/fdyPoolOneClass.html",
            controller: 'FdyPoolOneClassCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPoolOneClass.js')
            },
        })
        .state("index.fdyPoolOneStudent", {
            url: "/instructor/fdyPoolOneStudent",
            templateUrl: "../../static/front/views/instructor/fdyPoolOneStudent.html",
            controller: 'FdyPoolOneStudentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPoolOneStudent.js')
            },
        })
        .state("index.fdyHelpPool", {
            url: "/instructor/fdyHelpPool",
            templateUrl: "../../static/front/views/instructor/fdyHelpPool.html",
            controller: 'FdyHelpPoolCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyHelpPool.js')
            },
        })
        .state("index.fdyHelpPoolOne", {
            url: "/instructor/fdyHelpPoolOne",
            templateUrl: "../../static/front/views/instructor/fdyHelpPoolOne.html",
            controller: 'FdyHelpPoolOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyHelpPoolOne.js')
            },
        })
        .state("index.fdyScholarship", {
            url: "/instructor/fdyScholarship",
            templateUrl: "../../static/front/views/instructor/fdyScholarship.html",
            controller: 'FdyScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyScholarship.js')
            },
        })
        .state("index.fdyScholarshipOne", {
            url: "/instructor/fdyScholarshipOne",
            templateUrl: "../../static/front/views/instructor/fdyScholarshipOne.html",
            controller: 'FdyScholarshipOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyScholarshipOne.js')
            },
        })
        .state("index.fdyGJLZHScholarship", {
            url: "/instructor/fdyGJLZHScholarship",
            templateUrl: "../../static/front/views/instructor/fdyGJLZHScholarship.html",
            controller: 'FdyGJLZHScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGJLZHScholarship.js')
            },
        })
        .state("index.fdyZHNSHScholarship", {
            url: "/instructor/fdyZHNSHScholarship",
            templateUrl: "../../static/front/views/instructor/fdyZHNSHScholarship.html",
            controller: 'FdyZHNSHScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyZHNSHScholarship.js')
            },
        })
        .state("index.fdyGZHScholarship", {
            url: "/instructor/fdyGZHScholarship",
            templateUrl: "../../static/front/views/instructor/fdyGZHScholarship.html",
            controller: 'FdyGZHScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGZHScholarship.js')
            },
        })
        .state("index.fdyGrant", {
            url: "/instructor/fdyGrant",
            templateUrl: "../../static/front/views/instructor/fdyGrant.html",
            controller: 'FdyGrantCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGrant.js')
            },
        })
        .state("index.fdyGrantRegister", {
            url: "/instructor/fdyGrantRegister",
            templateUrl: "../../static/front/views/instructor/fdyGrantRegister.html",
            controller: 'FdyGrantRegisterCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGrantRegister.js')
            },
        })
        .state("index.fdyGrantSHQForm", {
            url: "/instructor/fdyGrantSHQForm",
            templateUrl: "../../static/front/views/instructor/fdyGrantSHQForm.html",
            controller: 'FdyGrantSHQFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGrantSHQForm.js')
            },
        })
        .state("index.fdyGrantSHQOneStudent", {
            url: "/instructor/fdyGrantSHQOneStudent",
            templateUrl: "../../static/front/views/instructor/fdyGrantSHQOneStudent.html",
            controller: 'FdyGrantSHQOneStudentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGrantSHQOneStudent.js')
            },
        })
        .state("index.fdyGrantCheck", {
            url: "/instructor/fdyGrantCheck",
            templateUrl: "../../static/front/views/instructor/fdyGrantCheck.html",
            controller: 'FdyGrantCheckCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGrantCheck.js')
            },
        })
        .state("index.fdyGrantCheckOne", {
            url: "/instructor/fdyGrantCheckOne",
            templateUrl: "../../static/front/views/instructor/fdyGrantCheckOne.html",
            controller: 'FdyGrantCheckOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyGrantCheckOne.js')
            },
        })
        .state("index.fdyHonor", {
            url: "/instructor/fdyHonor",
            templateUrl: "../../static/front/views/instructor/fdyHonor.html",
            controller: 'FdyHonorCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyHonor.js')
            },
        })
        .state("index.fdyHonorOne", {
            url: "/instructor/fdyHonorOne",
            templateUrl: "../../static/front/views/instructor/fdyHonorOne.html",
            controller: 'FdyHonorOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyHonorOne.js')
            },
        })
        .state("index.fdyClassLeader", {
            url: "/instructor/fdyClassLeader",
            templateUrl: "../../static/front/views/instructor/fdyClassLeader.html",
            controller: 'FdyClassLeaderCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyClassLeader.js')
            },
        })
        .state("index.fdyClassLeaderOne", {
            url: "/instructor/fdyClassLeaderOne",
            templateUrl: "../../static/front/views/instructor/fdyClassLeaderOne.html",
            controller: 'FdyClassLeaderOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyClassLeaderOne.js')
            },
        })
        .state("index.fdyOtherHonor", {
            url: "/instructor/fdyOtherHonor",
            templateUrl: "../../static/front/views/instructor/fdyOtherHonor.html",
            controller: 'FdyOtherHonorCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyOtherHonor.js')
            },
        })
        .state("index.fdyWorkStudy", {
            url: "/instructor/fdyWorkStudy",
            templateUrl: "../../static/front/views/instructor/fdyWorkStudy.html",
            controller: 'FdyWorkStudyCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyWorkStudy.js')
            },
        })
        .state("index.fdyWorkStudyOne", {
            url: "/instructor/fdyWorkStudyOne",
            templateUrl: "../../static/front/views/instructor/fdyWorkStudyOne.html",
            controller: 'FdyWorkStudyOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyWorkStudyOne.js')
            },
        })
        .state("index.fdyPunishment", {
            url: "/instructor/fdyPunishment",
            templateUrl: "../../static/front/views/instructor/fdyPunishment.html",
            controller: 'FdyPunishmentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPunishment.js')
            },
        })
        .state("index.fdyPunishmentOne", {
            url: "/instructor/fdyPunishmentOne",
            templateUrl: "../../static/front/views/instructor/fdyPunishmentOne.html",
            controller: 'FdyPunishmentOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPunishmentOne.js')
            },
        })
        .state("index.fdyPsychology", {
            url: "/instructor/fdyPsychology",
            templateUrl: "../../static/front/views/instructor/fdyPsychology.html",
            controller: 'FdyPsychologyCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPsychology.js')
            },
        })
        .state("index.fdyPsychologyOne", {
            url: "/instructor/fdyPsychologyOne",
            templateUrl: "../../static/front/views/instructor/fdyPsychologyOne.html",
            controller: 'FdyPsychologyOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPsychologyOne.js')
            },
        })
        .state("index.fdyPsychologyInter", {
            url: "/instructor/fdyPsychologyInter",
            templateUrl: "../../static/front/views/instructor/fdyPsychologyInter.html",
            controller: 'FdyPsychologyInterCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPsychologyInter.js')
            },
        })
        .state("index.fdyPsychologyInterOne", {
            url: "/instructor/fdyPsychologyInterOne",
            templateUrl: "../../static/front/views/instructor/fdyPsychologyInterOne.html",
            controller: 'FdyPsychologyInterOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdyPsychologyInterOne.js')
            },
        })
        .state("index.fdySafe", {
            url: "/instructor/fdySafe",
            templateUrl: "../../static/front/views/instructor/fdySafe.html",
            controller: 'FdySafeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdySafe.js')
            },
        })
        .state("index.fdySafeSelfCheck", {
            url: "/instructor/fdySafeSelfCheck",
            templateUrl: "../../static/front/views/instructor/fdySafeSelfCheck.html",
            controller: 'FdySafeSelfCheckCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdySafeSelfCheck.js')
            },
        })
        .state("index.fdySafeSelfCheckOne", {
            url: "/instructor/fdySafeSelfCheckOne",
            templateUrl: "../../static/front/views/instructor/fdySafeSelfCheckOne.html",
            controller: 'FdySafeSelfCheckOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/instructor/fdySafeSelfCheckOne.js')
            },
        })
        // 学生科
        .state("index.studentDepartment", {
            url: "/studentDepartment",
            templateUrl: "../../static/front/views/studentDepartment/index.html",
            controller: 'StudentDepartmentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/index.js')
            },
        })
        .state("index.sdManagement", {
            url: "/studentDepartment/sdManagement",
            templateUrl: "../../static/front/views/studentDepartment/sdManagement.html",
            controller: 'SdManagementCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdManagement.js')
            },
        })
        .state("index.sdInstructor", {
            url: "/studentDepartment/sdInstructor",
            templateUrl: "../../static/front/views/studentDepartment/sdInstructor.html",
            controller: 'SdInstructorCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdInstructor.js')
            },
        })
        .state("index.sdManagementOne", {
            url: "/studentDepartment/sdManagementOne",
            templateUrl: "../../static/front/views/studentDepartment/sdManagementOne.html",
            controller: 'SdManagementOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdManagementOne.js')
            },
        })
        .state("index.sdManaOneStudent", {
            url: "/studentDepartment/sdManaOneStudent",
            templateUrl: "../../static/front/views/studentDepartment/sdManaOneStudent.html",
            controller: 'SdManaOneStudentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdManaOneStudent.js')
            },
        })
        .state("index.sdInstructorOne", {
            url: "/studentDepartment/sdInstructorOne",
            templateUrl: "../../static/front/views/studentDepartment/sdInstructorOne.html",
            controller: 'SdInstructorOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdInstructorOne.js')
            },
        })
        .state("index.sdInstructorOneJob", {
            url: "/studentDepartment/sdInstructorOneJob",
            templateUrl: "../../static/front/views/studentDepartment/sdInstructorOneJob.html",
            controller: 'SdInstructorOneJobCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdInstructorOneJob.js')
            },
        })
        .state("index.sdHeadmaster", {
            url: "/studentDepartment/sdHeadmaster",
            templateUrl: "../../static/front/views/studentDepartment/sdHeadmaster.html",
            controller: 'SdHeadmasterCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdHeadmaster.js')
            },
        })
        .state("index.sdNatiMana", {
            url: "/studentDepartment/sdNatiMana",
            templateUrl: "../../static/front/views/studentDepartment/sdNatiMana.html",
            controller: 'SdNatiManaCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdNatiMana.js')
            },
        })
        .state("index.sdNotiMFile", {
            url: "/studentDepartment/sdNotiMFile",
            templateUrl: "../../static/front/views/studentDepartment/sdNotiMFile.html",
            controller: 'SdNotiMFileCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdNotiMFile.js')
            },
        })
        .state("index.sdNotiMFiOne", {
            url: "/studentDepartment/sdNotiMFiOne",
            templateUrl: "../../static/front/views/studentDepartment/sdNotiMFiOne.html",
            controller: 'SdNotiMFiOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdNotiMFiOne.js')
            },
        })
        .state("index.sdNotiMFiWord", {
            url: "/studentDepartment/sdNotiMFiWord",
            templateUrl: "../../static/front/views/studentDepartment/sdNotiMFiWord.html",
            controller: 'SdNotiMFiWordCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdNotiMFiWord.js')
            },
        })
        .state("index.sdPoolInfo", {
            url: "/studentDepartment/sdPoolInfo",
            templateUrl: "../../static/front/views/studentDepartment/sdPoolInfo.html",
            controller: 'SdPoolInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPoolInfo.js')
            },
        })
        .state("index.sdPoolIfOne", {
            url: "/studentDepartment/sdPoolIfOne",
            templateUrl: "../../static/front/views/studentDepartment/sdPoolIfOne.html",
            controller: 'SdPoolIfOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPoolIfOne.js')
            },
        })
        .state("index.sdHelpPool", {
            url: "/studentDepartment/sdHelpPool",
            templateUrl: "../../static/front/views/studentDepartment/sdHelpPool.html",
            controller: 'SdHelpPoolCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdHelpPool.js')
            },
        })
        .state("index.sdHelpPlOne", {
            url: "/studentDepartment/sdHelpPlOne",
            templateUrl: "../../static/front/views/studentDepartment/sdHelpPlOne.html",
            controller: 'SdHelpPlOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdHelpPlOne.js')
            },
        })
        .state("index.sdScholarship", {
            url: "/studentDepartment/sdScholarship",
            templateUrl: "../../static/front/views/studentDepartment/sdScholarship.html",
            controller: 'SdScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdScholarship.js')
            },
        })
        .state("index.sdScholarshipOne", {
            url: "/studentDepartment/sdScholarshipOne",
            templateUrl: "../../static/front/views/studentDepartment/sdScholarshipOne.html",
            controller: 'SdScholarshipOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdScholarshipOne.js')
            },
        })
        .state("index.sdGZHScholarship", {
            url: "/studentDepartment/sdGZHScholarship",
            templateUrl: "../../static/front/views/studentDepartment/sdGZHScholarship.html",
            controller: 'SdGZHScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGZHScholarship.js')
            },
        })
        .state("index.sdGJScholarship", {
            url: "/studentDepartment/sdGJScholarship",
            templateUrl: "../../static/front/views/studentDepartment/sdGJScholarship.html",
            controller: 'SdGJScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGJScholarship.js')
            },
        })
        .state("index.sdZHScholarship", {
            url: "/studentDepartment/sdZHScholarship",
            templateUrl: "../../static/front/views/studentDepartment/sdZHScholarship.html",
            controller: 'SdZHScholarshipCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdZHScholarship.js')
            },
        })
        .state("index.sdGrant", {
            url: "/studentDepartment/sdGrant",
            templateUrl: "../../static/front/views/studentDepartment/sdGrant.html",
            controller: 'SdGrantCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGrant.js')
            },
        })
        .state("index.sdGrantCheck", {
            url: "/studentDepartment/sdGrantCheck",
            templateUrl: "../../static/front/views/studentDepartment/sdGrantCheck.html",
            controller: 'SdGrantCheckCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGrantCheck.js')
            },
        })
        .state("index.sdGrantCheckOne", {
            url: "/studentDepartment/sdGrantCheckOne",
            templateUrl: "../../static/front/views/studentDepartment/sdGrantCheckOne.html",
            controller: 'SdGrantCheckOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGrantCheckOne.js')
            },
        })
        .state("index.sdGrantRegister", {
            url: "/studentDepartment/sdGrantRegister",
            templateUrl: "../../static/front/views/studentDepartment/sdGrantRegister.html",
            controller: 'SdGrantRegisterCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGrantRegister.js')
            },
        })
        .state("index.sdGrantSHQForm", {
            url: "/studentDepartment/sdGrantSHQForm",
            templateUrl: "../../static/front/views/studentDepartment/sdGrantSHQForm.html",
            controller: 'SdGrantSHQFormCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGrantSHQForm.js')
            },
        })
        .state("index.sdGrantSHQOneStudent", {
            url: "/studentDepartment/sdGrantSHQOneStudent",
            templateUrl: "../../static/front/views/studentDepartment/sdGrantSHQOneStudent.html",
            controller: 'SdGrantSHQOneStudentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdGrantSHQOneStudent.js')
            },
        })
        .state("index.sdHonor", {
            url: "/studentDepartment/sdHonor",
            templateUrl: "../../static/front/views/studentDepartment/sdHonor.html",
            controller: 'SdHonorCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdHonor.js')
            },
        })
        .state("index.sdHonorOne", {
            url: "/studentDepartment/sdHonorOne",
            templateUrl: "../../static/front/views/studentDepartment/sdHonorOne.html",
            controller: 'SdHonorOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdHonorOne.js')
            },
        })
        .state("index.sdClassLeader", {
            url: "/studentDepartment/sdClassLeader",
            templateUrl: "../../static/front/views/studentDepartment/sdClassLeader.html",
            controller: 'SdClassLeaderCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdClassLeader.js')
            },
        })
        .state("index.sdClassLeaderOne", {
            url: "/studentDepartment/sdClassLeaderOne",
            templateUrl: "../../static/front/views/studentDepartment/sdClassLeaderOne.html",
            controller: 'SdClassLeaderOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdClassLeaderOne.js')
            },
        })
        .state("index.sdOtherHonor", {
            url: "/studentDepartment/sdOtherHonor",
            templateUrl: "../../static/front/views/studentDepartment/sdOtherHonor.html",
            controller: 'SdOtherHonorCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdOtherHonor.js')
            },
        })
        .state("index.sdPunishment", {
            url: "/studentDepartment/sdPunishment",
            templateUrl: "../../static/front/views/studentDepartment/sdPunishment.html",
            controller: 'SdPunishmentCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPunishment.js')
            },
        })
        .state("index.sdPunishmentOne", {
            url: "/studentDepartment/sdPunishmentOne",
            templateUrl: "../../static/front/views/studentDepartment/sdPunishmentOne.html",
            controller: 'SdPunishmentOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPunishmentOne.js')
            },
        })
        .state("index.sdPsychology", {
            url: "/studentDepartment/sdPsychology",
            templateUrl: "../../static/front/views/studentDepartment/sdPsychology.html",
            controller: 'SdPsychologyCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPsychology.js')
            },
        })
        .state("index.sdPsychologyInter", {
            url: "/studentDepartment/sdPsychologyInter",
            templateUrl: "../../static/front/views/studentDepartment/sdPsychologyInter.html",
            controller: 'SdPsychologyInterCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPsychologyInter.js')
            },
        })
        .state("index.sdPsychologyInterOne", {
            url: "/studentDepartment/sdPsychologyInterOne",
            templateUrl: "../../static/front/views/studentDepartment/sdPsychologyInterOne.html",
            controller: 'SdPsychologyInterOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPsychologyInterOne.js')
            },
        })
        .state("index.sdPsychologyOne", {
            url: "/studentDepartment/sdPsychologyOne",
            templateUrl: "../../static/front/views/studentDepartment/sdPsychologyOne.html",
            controller: 'SdPsychologyOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdPsychologyOne.js')
            },
        })
        .state("index.sdSafe", {
            url: "/studentDepartment/sdSafe",
            templateUrl: "../../static/front/views/studentDepartment/sdSafe.html",
            controller: 'SdSafeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdSafe.js')
            },
        })
        .state("index.sdSafeSelfCheck", {
            url: "/studentDepartment/sdSafeSelfCheck",
            templateUrl: "../../static/front/views/studentDepartment/sdSafeSelfCheck.html",
            controller: 'SdSafeSelfCheckCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdSafeSelfCheck.js')
            },
        })
        .state("index.sdAllInfo", {
            url: "/studentDepartment/sdAllInfo",
            templateUrl: "../../static/front/views/studentDepartment/sdAllInfo.html",
            controller: 'SdAllInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdAllInfo.js')
            },
        })
        .state("index.sdAllInfoOne", {
            url: "/studentDepartment/sdAllInfoOne",
            templateUrl: "../../static/front/views/studentDepartment/sdAllInfoOne.html",
            controller: 'SdAllInfoOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/studentDepartment/sdAllInfoOne.js')
            },
        })
        // 科研管理-教职员工
        .state("index.srmNoti", {
            url: "/scientificResearchMana/srmNoti",
            templateUrl: "../../static/front/views/sResearchMana/srmNoti.html",
            controller: 'SrmNotiCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/srmNoti.js')
            },
        })
        .state("index.srmStaffIndex", {
            url: "/scientificResearchMana/srmStaffIndex",
            templateUrl: "../../static/front/views/sResearchMana/staff/index.html",
            controller: 'SrmStaffIndexCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/index.js')
            },
        })
        .state("index.srmStfNoti", {
            url: "/scientificResearchMana/srmStfNoti",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmStfNoti.html",
            controller: 'SrmStfNotiCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmStfNoti.js')
            },
        })
        .state("index.srmStfNotiOne", {
            url: "/scientificResearchMana/srmStfNotiOne",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmStfNotiOne.html",
            controller: 'SrmStfNotiOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmStfNotiOne.js')
            },
        })
        .state("index.srmNeedToDo", {
            url: "/scientificResearchMana/srmNeedToDo",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmNeedToDo.html",
            controller: 'SrmNeedToDoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmNeedToDo.js')
            },
        })
        .state("index.srmStatistics", {
            url: "/scientificResearchMana/srmStatistics",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmStatistics.html",
            controller: 'SrmStatisticsCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmStatistics.js')
            },
        })
        // 项目申报
        .state("index.srmProApplica", {
            url: "/scientificResearchMana/srmProApplica",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProApplica.html",
            controller: 'SrmProApplicaCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProApplica.js')
            },
        })
        // 新建项目汇报
        .state("index.srmNewPro", {
            url: "/scientificResearchMana/srmNewPro",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmNewPro.html",
            controller: 'SrmNewProCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmNewPro.js')
            },
        })
        // 查看项目审核通过
        .state("index.srmCheckPro", {
            url: "/scientificResearchMana/srmCheckPro",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmCheckPro.html",
            controller: 'SrmCheckProCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmCheckPro.js')
            },
        })
        // 查看项目-审核不通过
        .state("index.srmCheckProNo", {
            url: "/scientificResearchMana/srmCheckProNo",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmCheckProNo.html",
            controller: 'SrmCheckProNoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmCheckProNo.js')
            },
        })
        // 查看项目-审核不通过-修改项目
        .state("index.srmChangePro", {
            url: "/scientificResearchMana/srmChangePro",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmChangePro.html",
            controller: 'SrmChangeProCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmChangePro.js')
            },
        })
        // 纵向项目获得
        .state("index.srmProGet", {
            url: "/scientificResearchMana/srmProGet",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProGet.html",
            controller: 'SrmProGetCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProGet.js')
            },
        })
        // 纵向项目获得-查看
        .state("index.srmProGetCheck", {
            url: "/scientificResearchMana/srmProGetCheck/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProGetCheck.html",
            controller: 'SrmProGetCheckCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProGetCheck.js')
            },
        })
        // 横向项目获得
        .state("index.srmProGetH", {
            url: "/scientificResearchMana/srmProGetH",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProGetH.html",
            controller: 'SrmProGetHCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProGetH.js')
            },
        })
        // 横向项目获得-查看
        .state("index.srmProGetCheckH", {
            url: "/scientificResearchMana/srmProGetCheckH/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProGetCheckH.html",
            controller: 'SrmProGetCheckHCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProGetCheckH.js')
            },
        })
        // 项目成果-论文
        .state("index.srmProResul", {
            url: "/scientificResearchMana/srmProResul",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResul.html",
            controller: 'SrmProResulCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResul.js')
            },
        })
        // 项目成果-论文-查看
        .state("index.srmProResulCh", {
            url: "/scientificResearchMana/srmProResulCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulCh.html",
            controller: 'SrmProResulChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulCh.js')
            },
        })
        // 项目成果-获奖
        .state("index.srmProResulHJ", {
            url: "/scientificResearchMana/srmProResulHJ",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulHJ.html",
            controller: 'SrmProResulHJCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulHJ.js')
            },
        })
        // 项目成果-获奖-查看
        .state("index.srmProResulHJCh", {
            url: "/scientificResearchMana/srmProResulHJCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulHJCh.html",
            controller: 'SrmProResulHJChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulHJCh.js')
            },
        })
        // 项目成果-专著
        .state("index.srmProResulZHZHU", {
            url: "/scientificResearchMana/srmProResulZHZHU",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulZHZHU.html",
            controller: 'SrmProResulZHZHUCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulZHZHU.js')
            },
        })
        // 项目成果-专著-查看
        .state("index.srmProResulZHZHUCh", {
            url: "/scientificResearchMana/srmProResulZHZHUCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulZHZHUCh.html",
            controller: 'SrmProResulZHZHUChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulZHZHUCh.js')
            },
        })
        // 项目成果-专利
        .state("index.srmProResulZHL", {
            url: "/scientificResearchMana/srmProResulZHL",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulZHL.html",
            controller: 'SrmProResulZHLCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulZHL.js')
            },
        })
        // 项目成果-专利-查看
        .state("index.srmProResulZHLCh", {
            url: "/scientificResearchMana/srmProResulZHLCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulZHLCh.html",
            controller: 'SrmProResulZHLChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulZHLCh.js')
            },
        })
        // 项目成果-咨询报告
        .state("index.srmProResulZX", {
            url: "/scientificResearchMana/srmProResulZX",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulZX.html",
            controller: 'SrmProResulZXCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulZX.js')
            },
        })
        // 项目成果-咨询报告-查看
        .state("index.srmProResulZXCh", {
            url: "/scientificResearchMana/srmProResulZXCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulZXCh.html",
            controller: 'SrmProResulZXChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulZXCh.js')
            },
        })
        // 项目成果-成果转化
        .state("index.srmProResulCHG", {
            url: "/scientificResearchMana/srmProResulCHG",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulCHG.html",
            controller: 'SrmProResulCHGCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulCHG.js')
            },
        })
        // 项目成果-成果转化-查看
        .state("index.srmProResulCHGCh", {
            url: "/scientificResearchMana/srmProResulCHGCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulCHGCh.html",
            controller: 'SrmProResulCHGChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulCHGCh.js')
            },
        })
        // 项目成果-国内外学术会议报告
        .state("index.srmProResulGB", {
            url: "/scientificResearchMana/srmProResulGB",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulGB.html",
            controller: 'SrmProResulGBCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulGB.js')
            },
        })
        // 项目成果-国内外学术会议报告-查看
        .state("index.srmProResulGBCh", {
            url: "/scientificResearchMana/srmProResulGBCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulGBCh.html",
            controller: 'SrmProResulGBChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulGBCh.js')
            },
        })
        // 项目成果-国内外参加学术会议
        .state("index.srmProResulGC", {
            url: "/scientificResearchMana/srmProResulGC",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulGC.html",
            controller: 'SrmProResulGCCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulGC.js')
            },
        })
        // 项目成果-国内外参加学术会议-查看
        .state("index.srmProResulGCCh", {
            url: "/scientificResearchMana/srmProResulGCCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulGCCh.html",
            controller: 'SrmProResulGCChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulGCCh.js')
            },
        })
        // 项目成果-国外进修（合作研究）
        .state("index.srmProResulGWJ", {
            url: "/scientificResearchMana/srmProResulGWJ",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulGWJ.html",
            controller: 'SrmProResulGWJCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulGWJ.js')
            },
        })
        // 项目成果-国外进修（合作研究）-查看
        .state("index.srmProResulGWJCh", {
            url: "/scientificResearchMana/srmProResulGWJCh/:proId",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmProResulGWJCh.html",
            controller: 'SrmProResulGWJChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmProResulGWJCh.js')
            },
        })
        // 个人资料
        .state("index.srmSelfInfo", {
            url: "/scientificResearchMana/srmSelfInfo",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmSelfInfo.html",
            controller: 'SrmSelfInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmSelfInfo.js')
            },
        })
        // 汇总导出
        .state("index.srmTotal", {
            url: "/scientificResearchMana/srmTotal",
            templateUrl: "../../static/front/views/sResearchMana/staff/srmTotal.html",
            controller: 'SrmTotalCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/sResearchMana/staff/srmTotal.js')
            },
        })
        // 教学模块
        .state("index.teaching", {
            url: "/teaching",
            templateUrl: "../../static/front/views/teaching/index.html",
            controller: 'TeachingCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/index.js')
            },
        })
        // 教学模块-公告管理
        .state("index.teNotice", {
            url: "/teaching/teNotice",
            templateUrl: "../../static/front/views/teaching/teNotice.html",
            controller: 'TeNoticeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teNotice.js')
            },
        })
        // 教学模块-公告管理-详情
        .state("index.teNoticeInfo", {
            url: "/teaching/teNoticeInfo",
            templateUrl: "../../static/front/views/teaching/teNoticeInfo.html",
            controller: 'TeNoticeInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teNoticeInfo.js')
            },
        })
        // 教学模块-公告管理-新建/编辑
        .state("index.teNoticeInfoOne", {
            url: "/teaching/teNoticeInfoOne/:proId",
            templateUrl: "../../static/front/views/teaching/teNoticeInfoOne.html",
            controller: 'TeNoticeInfoOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teNoticeInfoOne.js')
            },
        })
        // 教学模块-教学改革与教学成果-大学生创新创业项目
        .state("index.teAchievStuPro", {
            url: "/teaching/teAchievStuPro",
            templateUrl: "../../static/front/views/teaching/teAchievStuPro.html",
            controller: 'TeAchievStuProCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teAchievStuPro.js')
            },
        })
        // 教学模块-教学改革与教学成果-大学生创新创业项目-查看
        .state("index.teAchievStuProCh", {
            url: "/teaching/teAchievStuProCh/:proId",
            templateUrl: "../../static/front/views/teaching/teAchievStuProCh.html",
            controller: 'TeAchievStuProChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teAchievStuProCh.js')
            },
        })
        // 教学模块-教学改革与教学成果-教学研究论文
        .state("index.teResearchPaper", {
            url: "/teaching/teResearchPaper",
            templateUrl: "../../static/front/views/teaching/teResearchPaper.html",
            controller: 'TeResearchPaperCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teResearchPaper.js')
            },
        })
        // 教学模块-教学改革与教学成果-教学研究论文-查看
        .state("index.teResearchPaperCh", {
            url: "/teaching/teResearchPaperCh/:proId",
            templateUrl: "../../static/front/views/teaching/teResearchPaperCh.html",
            controller: 'TeResearchPaperChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teResearchPaperCh.js')
            },
        })
        // 教学模块-教学改革与教学成果-教学改革项目
        .state("index.teEduGaige", {
            url: "/teaching/teEduGaige",
            templateUrl: "../../static/front/views/teaching/teEduGaige.html",
            controller: 'TeEduGaigeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teEduGaige.js')
            },
        })
        // 教学模块-教学改革与教学成果-教学改革项目-查看
        .state("index.teEduGaigeCh", {
            url: "/teaching/teEduGaigeCh/:proId",
            templateUrl: "../../static/front/views/teaching/teEduGaigeCh.html",
            controller: 'TeEduGaigeChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teEduGaigeCh.js')
            },
        })
        // 教学模块-教学改革与教学成果-教材建设
        .state("index.teMetCons", {
            url: "/teaching/teMetCons",
            templateUrl: "../../static/front/views/teaching/teMetCons.html",
            controller: 'TeMetConsCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teMetCons.js')
            },
        })
        // 教学模块-教学改革与教学成果-教材建设-查看
        .state("index.teMetConsCh", {
            url: "/teaching/teMetConsCh/:proId",
            templateUrl: "../../static/front/views/teaching/teMetConsCh.html",
            controller: 'TeMetConsChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teMetConsCh.js')
            },
        })
        // 教学模块-教学改革与教学成果-教学获奖
        .state("index.teTeWin", {
            url: "/teaching/teTeWin",
            templateUrl: "../../static/front/views/teaching/teTeWin.html",
            controller: 'TeTeWinCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teTeWin.js')
            },
        })
        // 教学模块-教学改革与教学成果-教学获奖-查看
        .state("index.teTeWinCh", {
            url: "/teaching/teTeWinCh/:proId",
            templateUrl: "../../static/front/views/teaching/teTeWinCh.html",
            controller: 'TeTeWinChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teTeWinCh.js')
            },
        })
        // 教学模块-实习管理-实习管理
        .state("index.tePracMana", {
            url: "/teaching/tePracMana",
            templateUrl: "../../static/front/views/teaching/tePracMana.html",
            controller: 'TePracManaCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/tePracMana.js')
            },
        })
        // 教学模块-实习管理-实习管理-查看
        .state("index.tePracManaCh", {
            url: "/teaching/tePracManaCh/:proId",
            templateUrl: "../../static/front/views/teaching/tePracManaCh.html",
            controller: 'TePracManaChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/tePracManaCh.js')
            },
        })
        // 教学模块-实习管理-学生实习
        .state("index.teStuPrac", {
            url: "/teaching/teStuPrac",
            templateUrl: "../../static/front/views/teaching/teStuPrac.html",
            controller: 'TeStuPracCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teStuPrac.js')
            },
        })
        // 教学模块-实习管理-论文指导
        .state("index.tePaperGuid", {
            url: "/teaching/tePaperGuid",
            templateUrl: "../../static/front/views/teaching/tePaperGuid.html",
            controller: 'TePaperGuidCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/tePaperGuid.js')
            },
        })
        // 教学模块-毕业管理-开题信息
        .state("index.teGraMaTitle", {
            url: "/teaching/teGraMaTitle",
            templateUrl: "../../static/front/views/teaching/teGraMaTitle.html",
            controller: 'TeGraMaTitleCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teGraMaTitle.js')
            },
        })
        // 教学模块-毕业管理-开题信息-查看
        .state("index.teGraMaTitleCh", {
            url: "/teaching/teGraMaTitleCh/:proId",
            templateUrl: "../../static/front/views/teaching/teGraMaTitleCh.html",
            controller: 'TeGraMaTitleChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teGraMaTitleCh.js')
            },
        })
        // 教学模块-毕业管理-答辩管理-答辩小组
        .state("index.teDefManTeam", {
            url: "/teaching/teDefManTeam",
            templateUrl: "../../static/front/views/teaching/teDefManTeam.html",
            controller: 'TeDefManTeamCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teDefManTeam.js')
            },
        })
        // 教学模块-毕业管理-答辩管理-答辩小组-查看
        .state("index.teDefManTeamCh", {
            url: "/teaching/teDefManTeamCh/:proId",
            templateUrl: "../../static/front/views/teaching/teDefManTeamCh.html",
            controller: 'TeDefManTeamChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teDefManTeamCh.js')
            },
        })
        // 教学模块-毕业管理-答辩管理-答辩成绩
        .state("index.teDefAchiev", {
            url: "/teaching/teDefAchiev",
            templateUrl: "../../static/front/views/teaching/teDefAchiev.html",
            controller: 'TeDefAchievCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teDefAchiev.js')
            },
        })
        // 教学模块-毕业管理-答辩管理-答辩成绩-查看
        .state("index.teDefAchievCh", {
            url: "/teaching/teDefAchievCh/:proId",
            templateUrl: "../../static/front/views/teaching/teDefAchievCh.html",
            controller: 'TeDefAchievChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teDefAchievCh.js')
            },
        })
        // 教学模块-毕业管理-毕业成绩
        .state("index.teGraduAchiev", {
            url: "/teaching/teGraduAchiev",
            templateUrl: "../../static/front/views/teaching/teGraduAchiev.html",
            controller: 'TeGraduAchievCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teGraduAchiev.js')
            },
        })
        // 教学模块-导师管理-导师信息
        .state("index.teTutorIf", {
            url: "/teaching/teTutorIf",
            templateUrl: "../../static/front/views/teaching/teTutorIf.html",
            controller: 'TeTutorIfCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teTutorIf.js')
            },
        })
        // 教学模块-导师管理-导师信息-查看
        .state("index.teTutorIfCh", {
            url: "/teaching/teTutorIfCh/:proId",
            templateUrl: "../../static/front/views/teaching/teTutorIfCh.html",
            controller: 'TeTutorIfChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teTutorIfCh.js')
            },
        })
        // 教学模块-导师管理-学生信息
        .state("index.teTutorStuIf", {
            url: "/teaching/teTutorStuIf",
            templateUrl: "../../static/front/views/teaching/teTutorStuIf.html",
            controller: 'TeTutorStuIfCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teTutorStuIf.js')
            },
        })
        // 教学模块-导师管理-学生信息-查看
        .state("index.teTutorStuIfCh", {
            url: "/teaching/teTutorStuIfCh/:proId",
            templateUrl: "../../static/front/views/teaching/teTutorStuIfCh.html",
            controller: 'TeTutorStuIfChCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teTutorStuIfCh.js')
            },
        })
        // 教学模块-个人资料
        .state("index.teSelfInfo", {
            url: "/teaching/teSelfInfo",
            templateUrl: "../../static/front/views/teaching/teSelfInfo.html",
            controller: 'TeSelfInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfInfo.js')
            },
        })
        // 教学模块-汇总导出
        .state("index.teTotal", {
            url: "/teaching/teTotal",
            templateUrl: "../../static/front/views/teaching/teTotal.html",
            controller: 'TeTotalCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teTotal.js')
            },
        })
        // 教学模块-我的实习
        .state("index.teInternshipCom", {
            url: "/teaching/teInternshipCom",
            templateUrl: "../../static/front/views/teaching/teInternshipCom.html",
            controller: 'TeInternshipComCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teInternshipCom.js')
            },
        })
        // 教学模块-我的实习-选择实习单位
        .state("index.teChioceCom", {
            url: "/teaching/teChioceCom",
            templateUrl: "../../static/front/views/teaching/teChioceCom.html",
            controller: 'TeChioceComCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teChioceCom.js')
            },
        })
        // 教学模块-我的实习-论文指导
        .state("index.teInComPaperGui", {
            url: "/teaching/teInComPaperGui",
            templateUrl: "../../static/front/views/teaching/teInComPaperGui.html",
            controller: 'TeInComPaperGuiCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teInComPaperGui.js')
            },
        })
        // 教学模块-我的毕业-开题信息
        .state("index.teSelfPropInfo", {
            url: "/teaching/teSelfPropInfo",
            templateUrl: "../../static/front/views/teaching/teSelfPropInfo.html",
            controller: 'TeSelfPropInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfPropInfo.js')
            },
        })
        // 教学模块-我的毕业-预答辩
        .state("index.teSelfPreProp", {
            url: "/teaching/teSelfPreProp",
            templateUrl: "../../static/front/views/teaching/teSelfPreProp.html",
            controller: 'TeSelfPrePropCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfPreProp.js')
            },
        })
        // 教学模块-我的毕业-答辩
        .state("index.teSelfDabian", {
            url: "/teaching/teSelfDabian",
            templateUrl: "../../static/front/views/teaching/teSelfDabian.html",
            controller: 'TeSelfDabianCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfDabian.js')
            },
        })
        // 教学模块-我的毕业-二次答辩
        .state("index.teSelfSecDab", {
            url: "/teaching/teSelfSecDab",
            templateUrl: "../../static/front/views/teaching/teSelfSecDab.html",
            controller: 'TeSelfSecDabCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfSecDab.js')
            },
        })
        // 教学模块-我的毕业-毕业成绩
        .state("index.teSelfGradAchiev", {
            url: "/teaching/teSelfGradAchiev",
            templateUrl: "../../static/front/views/teaching/teSelfGradAchiev.html",
            controller: 'TeSelfGradAchievCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfGradAchiev.js')
            },
        })
        // 教学模块-我的导师-导师信息
        .state("index.teSelfTutor", {
            url: "/teaching/teSelfTutor",
            templateUrl: "../../static/front/views/teaching/teSelfTutor.html",
            controller: 'TeSelfTutorCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfTutor.js')
            },
        })
        // 教学模块-我的导师-导师评价
        .state("index.teSelfEval", {
            url: "/teaching/teSelfEval",
            templateUrl: "../../static/front/views/teaching/teSelfEval.html",
            controller: 'TeSelfEvalCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teaching/teSelfEval.js')
            },
        })
        // 党务管理系统-登陆首页
        .state("index.ptAfManIndex", {
            url: "/partyAffMana",
            templateUrl: "../../static/front/views/partyAffMana/index.html",
            controller: 'PtAfManIndexCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/index.js')
            },
        })
        // 党务管理系统-公告管理
        .state("index.ptAfManNotice", {
            url: "/partyAffMana/ptAfManNotice",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManNotice.html",
            controller: 'PtAfManNoticeCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManNotice.js')
            },
        })
        // 党务管理系统-公告管理-查看一个公告
        .state("index.ptAfManNotiOne", {
            url: "/partyAffMana/ptAfManNotiOne",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManNotiOne.html",
            controller: 'PtAfManNotiOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManNotiOne.js')
            },
        })
        // 党务管理系统-公告管理-新建公告
        .state("index.ptAfManNewNoti", {
            url: "/partyAffMana/ptAfManNewNoti",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManNewNoti.html",
            controller: 'PtAfManNewNotiCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManNewNoti.js')
            },
        })

        // 党务管理系统-学习材料
        .state("index.ptAfManLeMate", {
            url: "/partyAffMana/ptAfManLeMate",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManLeMate.html",
            controller: 'PtAfManLeMateCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManLeMate.js')
            },
        })
        // 党务管理系统-学习材料-查看详情
        .state("index.ptAfManLeMateOne", {
            url: "/partyAffMana/ptAfManLeMateOne",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManLeMateOne.html",
            controller: 'PtAfManLeMateOneCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManLeMateOne.js')
            },
        })
        // 党务管理系统-学习材料-新建/编辑
        .state("index.ptAfManLeMateDet", {
            url: "/partyAffMana/ptAfManLeMateDet",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManLeMateDet.html",
            controller: 'PtAfManLeMateDetCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManLeMateDet.js')
            },
        })
        // 党务管理系统-学习日志
        .state("index.ptAfManLeLog", {
            url: "/partyAffMana/ptAfManLeLog",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManLeLog.html",
            controller: 'PtAfManLeLogCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManLeLog.js')
            },
        })
        // 党务管理系统-学习日志-学习日志查看
        .state("index.ptAfManNewLeLog", {
            url: "/partyAffMana/ptAfManNewLeLog",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManNewLeLog.html",
            controller: 'PtAfManNewLeLogCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManNewLeLog.js')
            },
        })
        // 党务管理系统-个人资料
        .state("index.ptAfManSelfInfo", {
            url: "/partyAffMana/ptAfManSelfInfo",
            templateUrl: "../../static/front/views/partyAffMana/ptAfManSelfInfo.html",
            controller: 'PtAfManSelfInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/partyAffMana/ptAfManSelfInfo.js')
            },
        })
        // 教职工模块--首页
        .state("index.teachStaff", {
            url: "/teachStaff",
            templateUrl: "../../static/front/views/teachStaff/index.html",
            controller: 'TeachStaffCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teachStaff/index.js')
            },
        })
        // 教职工模块--个人资料
        .state("index.tsSelfInfo", {
            url: "/teachStaff/tsSelfInfo",
            templateUrl: "../../static/front/views/teachStaff/tsSelfInfo.html",
            controller: 'TsSelfInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teachStaff/tsSelfInfo.js')
            },
        })
        // 教职工模块--个人资料--修改个人信息
        .state("index.tsChangeInfo", {
            url: "/teachStaff/tsChangeInfo",
            templateUrl: "../../static/front/views/teachStaff/tsChangeInfo.html",
            controller: 'TsChangeInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teachStaff/tsChangeInfo.js')
            },
        })
        // 教职工模块--个人资料--修改个人信息
        .state("index.tsAddInfo", {
            url: "/teachStaff/tsAddInfo",
            templateUrl: "../../static/front/views/teachStaff/tsAddInfo.html",
            controller: 'TsAddInfoCtrl',
            resolve: {
                loadMyFile: _lazyLoad('../../static/front/js/controllers/teachStaff/tsAddInfo.js')
            },
        })
        // 
        .state("index.AjaxTest", {
            url: "/AjaxTest",
            templateUrl: "../../static/front/views/AjaxTest.html"
        })
}
angular
    .module('inspinia')
    .config(config)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
        
        //监听路由变化
        $rootScope.$on('$stateChangeStart', function(){
            setTimeout(function(){
                $('.pos-commen-content-left').css('min-height', $('#page-wrapper').height() - 150  + "px");
                console.log($('.pos-commen-content-left').css('min-height'));
            }, 100)
        });
    });
