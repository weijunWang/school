
angular
.module('inspinia')
.controller('CheckWorkCtrl', CheckWorkCtrl)
function CheckWorkCtrl($scope,$rootScope){
    console.log('CheckWorkCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.leave = function(){
        location.href = '#/index/student/leave'
    }
    $scope.cancleLeave = function(){
        location.href = '#/index/student/cancelLeave'
    }
}