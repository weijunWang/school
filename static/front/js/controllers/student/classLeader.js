angular
.module('inspinia')
.controller('ClassLeaderCtrl', ClassLeaderCtrl)
function ClassLeaderCtrl($scope,$rootScope){
    console.log('ClassLeaderCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            text: '2017年度贵州医科大学优秀班干评选公告通知',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州省级优秀班干评选公告通知',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州省省级优秀班干申请表',
            // time: '2018.01.16'
        }
    ]
    $scope.toPage = function(){
        var url = '';
        switch(this.$index){
            case 0:
                url = '#/index/student/classLeaderForm';
                break;
            case 1:
                url = '#/index/student/classLeaderForm';
                break;
            case 2:
                url = '#/index/student/classLeaderForm';
                break;
            default:
                break;
        }
        location.href = url;
    }
}