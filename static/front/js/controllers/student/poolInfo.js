angular
.module('inspinia')
.controller('PoolInfoCtrl', PoolInfoCtrl)
function PoolInfoCtrl($scope,$rootScope){
    console.log('PoolInfoCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            text: '2017年度贵州医科大学精准扶贫精准扶贫公告通知',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学国家精准扶贫评选细则',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学国家精准扶贫申请表',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学国家精准扶贫贫困信息登记表',
            // time: '2018.01.16'
        }
    ]
    $scope.toPage = function(){
        console.log(this.$index);
        var url = null;
        switch(this.$index){
            case 0:
                url = '#/index/student/poolInfoNotice';
                break;
            case 1:
                url = '#/index/student/poolInfoRegister';
                break; 
            case 2:
                url = '#/index/student/poolAlleviation';
                break; 
            case 2:
                url = '#/index/student/poolAlleviation';
                break; 
            default:
                break;
        }
        location.href = url;
    }
    $scope.toSelfSupport = function(){
        location.href = '#/index/student/selfSupport'
    }
}