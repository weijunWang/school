angular
.module('inspinia')
.controller('PunishmentCtrl', PunishmentCtrl)
function PunishmentCtrl($scope,$rootScope){
    console.log('PunishmentCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            text: '2017年度贵州医科大学关于四六级英语等级考试作弊处罚通知',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学期末考试作弊处罚公告',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学有关处罚通知',
            // time: '2018.01.16'
        }
    ]
    $scope.toPage = function(){
        var url = '';
        switch(this.$index){
            case 0:
                url = '#/index/student/selfPunishment';
                break;
            case 1:
                url = '#/index/student/selfPunishment';
                break;
            case 2:
                url = '#/index/student/selfPunishment';
                break;
            default:
                break;
        }
        location.href = url;
    }
}