angular
.module('inspinia')
.controller('WhereaboutsCtrl', WhereaboutsCtrl)
function WhereaboutsCtrl($scope,$rootScope){
    console.log('WhereaboutsCtrl');
    $rootScope.student = true;
    $rootScope.myActive = 'active';
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.addWhereaboutsRegister = function(){
        location.href = '#/index/student/whereaboutsRegister'
    }
}