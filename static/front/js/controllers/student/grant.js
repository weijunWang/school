
angular
.module('inspinia')
.controller('GrantCtrl', GrantCtrl)
function GrantCtrl($scope,$rootScope){
    console.log('GrantCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            text: '2017年度贵州医科大学国家助学金评选公告通知 ',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学国家助学金评选细则',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学国家助学金申请表',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学家庭困难认定表',
            // time: '2018.01.16'
        }
    ]
    $scope.toPage = function(){
        console.log(this.$index)
        var url = null;
        switch(this.$index){
            case 0:
                url = '#/index/student/grantRule';
                break;
            case 1:
                url = '#/index/student/grantRule';
                break;
            case 2:
                url = '#/index/student/grantForm';
                break;
            case 3:
                url = '#/index/student/homePoolForm';
                break;
            default:
                break;
        }
        location.href = url;
    }
    $scope.toSelfGrant = function(){
        location.href = '#/index/student/selfGrant'
    }
    
}