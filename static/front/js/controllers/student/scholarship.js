
angular
.module('inspinia')
.controller('ScholarshipCtrl', ScholarshipCtrl)
function ScholarshipCtrl($scope,$rootScope){
    console.log('ScholarshipCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            text: '2017年度贵州医科大学国家奖学金评选公告通知 ',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学国家奖学金评选细则',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学国家奖学金申请表',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学励志奖学金评选公告通知',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学励志奖学金评选公告通知',
            // time: '2018.01.16'
        }
    ]
    $scope.toNationalScholarship = function(){
        location.href = '#/index/student/nationalScholarship'
    }
    $scope.toEncouragementScholarship = function(){
        location.href = '#/index/student/encouragementScholarship'
    }
    $scope.toZhongNanShanscholarship = function(){
        location.href = '#/index/student/zhongNanShanscholarship'
    }
    $scope.toSelfGuizhouScholarship = function(){
        location.href = '#/index/student/selfGuizhouScholarship'
    }
}