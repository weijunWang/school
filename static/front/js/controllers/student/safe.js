angular
.module('inspinia')
.controller('SafeCtrl', SafeCtrl)
function SafeCtrl($scope,$rootScope){
    console.log('SafeCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            text: '2017年度贵州医科大学假期安排公告通知',
            time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学宿舍卫生自查表',
            time: '2018.01.16'
        },{
            text: '2017年度贵州医科大学放假登记',
            time: '2018.01.16'
        }
    ]
    $scope.toSelfSafe = function(){
        location.href = '#/index/student/selfSafe';
    }
}