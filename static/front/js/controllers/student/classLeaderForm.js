angular
.module('inspinia')
.controller('ClassLeaderFormCtrl', ClassLeaderFormCtrl)
function ClassLeaderFormCtrl($scope,$rootScope){
    console.log('ClassLeaderFormCtrl');
    $rootScope.student = true;
    $scope.toSelfWorkStudy = function(){
        location.href = '#/index/student/selfWorkStudy'
    }
    $scope.toSelfClassLeader = function(){
        location.href = '#/index/student/selfClassLeader'
    }
}