angular
.module('inspinia')
.controller('SelfSafeCtrl', SelfSafeCtrl)
function SelfSafeCtrl($scope,$rootScope){
    console.log('SelfSafeCtrl');
    $rootScope.student = true;
    $scope.addSafe = function(){
        location.href = '#/index/student/addSafeForm';
    }
}