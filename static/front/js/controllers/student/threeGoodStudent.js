angular
.module('inspinia')
.controller('ThreeGoodStudentCtrl', ThreeGoodStudentCtrl)
function ThreeGoodStudentCtrl($scope,$rootScope){
    console.log('ThreeGoodStudentCtrl');
    $rootScope.student = true;
    $scope.allInfos = [
        {
            text: '2017年度贵州医科大学三好学生评选公告通知 ',
            // time: '2018.01.16'
        },{
            text: '2017年度贵州省省级三好学生评选公共通知申请表',
            // time: '2018.01.16'
        }
    ]
    $scope.toPage = function(){
        var url = '';
        switch(this.$index){
            case 0:
                url = '#/index/student/threeGoodStudentForm';
                break;
            case 1:
                url = '#/index/student/threeGoodStudentForm';
                break;
            default:
                break;
        }
        location.href = url;
    }
}