angular
.module('inspinia')
.controller('FdyHonorCtrl', FdyHonorCtrl)
function FdyHonorCtrl($scope,$rootScope, IndexInfo){
    console.log('FdyHonorCtrl');
    $rootScope.instructor = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toFdyOneStudent = function(){
        location.href = '#/index/instructor/fdyHonorOne'
    }
    $scope.toFdyClassLeader = function(){
        location.href = '#/index/instructor/fdyClassLeader';
    }
    $scope.toFdyOtherHonor = function(){
        location.href = '#/index/instructor/fdyOtherHonor';
    }
}