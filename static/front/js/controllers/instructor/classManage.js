angular
.module('inspinia')
.controller('ClassManageCtrl', ClassManageCtrl)
function ClassManageCtrl($scope,$rootScope, IndexInfo){
    console.log('ClassManageCtrl');
    $rootScope.instructor = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toPage = function(){
        console.log(this.$index);
        var url = '';
        switch(this.$index){
            case 0:
                url = '#/index/instructor/fdyOneClass';
                break;
            default:
                break;
        }
        location.href = url;
    }
    $scope.toFdyCheckWork = function(){
        location.href = '#/index/instructor/fdyCheckWork'
    }
    $scope.toFdyWorkLog = function(){
        location.href = '#/index/instructor/fdyWorkLog'
    }
}