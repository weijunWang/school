angular
.module('inspinia')
.controller('InstructorCtrl', InstructorCtrl)
function InstructorCtrl($scope,$rootScope, IndexInfo){
    console.log('InstructorCtrl');
    $rootScope.instructor = true;
    $rootScope.myActive = 'active';
    $scope.indexInfo = IndexInfo;
    $scope.changeHidden = function(){
        console.log(this.$index);
        for(var i = 0;i < $scope.indexInfo.length;i++){
            $scope.indexInfo[i].isHidden = false;
        }
        $scope.indexInfo[this.$index].isHidden = true;
    }
}