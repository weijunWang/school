angular
.module('inspinia')
.controller('FdyGrantCheckCtrl', FdyGrantCheckCtrl)
function FdyGrantCheckCtrl($scope,$rootScope, IndexInfo){
    console.log('FdyGrantCheckCtrl');
    $rootScope.instructor = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toFdyOneStudent = function(){
        location.href = '#/index/instructor/fdyGrantCheckOne'
    }
    $scope.toFdyGrantRegister = function(){
        location.href = '#/index/instructor/fdyGrantSHQForm';
    }
    $scope.toFdyZHNSHScholarship = function(){
        location.href = '#/index/instructor/fdyZHNSHScholarship';
    }
    $scope.toFdyGZHScholarship = function(){
        location.href = '#/index/instructor/fdyGZHScholarship';
    }
}