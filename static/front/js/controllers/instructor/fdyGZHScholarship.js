angular
.module('inspinia')
.controller('FdyGZHScholarshipCtrl', FdyGZHScholarshipCtrl)
function FdyGZHScholarshipCtrl($scope,$rootScope, IndexInfo){
    console.log('FdyGZHScholarshipCtrl');
    $rootScope.instructor = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toFdyOneStudent = function(){
        location.href = '#/index/instructor/fdyScholarshipOne';
    }
    $scope.toFdyGJLZHScholarship = function(){
        location.href = '#/index/instructor/fdyGJLZHScholarship';
    }
}