angular
.module('inspinia')
.controller('FdyScholarshipCtrl', FdyScholarshipCtrl)
function FdyScholarshipCtrl($scope,$rootScope, IndexInfo){
    console.log('FdyScholarshipCtrl');
    $rootScope.instructor = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toFdyOneStudent = function(){
        location.href = '#/index/instructor/fdyScholarshipOne'
    }
    $scope.toFdyGJLZHScholarship = function(){
        location.href = '#/index/instructor/fdyGJLZHScholarship';
    }
    $scope.toFdyZHNSHScholarship = function(){
        location.href = '#/index/instructor/fdyZHNSHScholarship';
    }
    $scope.toFdyGZHScholarship = function(){
        location.href = '#/index/instructor/fdyGZHScholarship';
    }
}