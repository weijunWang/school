angular
.module('inspinia')
.controller('FdyGrantSHQFormCtrl', FdyGrantSHQFormCtrl)
function FdyGrantSHQFormCtrl($scope,$rootScope, IndexInfo){
    console.log('FdyGrantSHQFormCtrl');
    $rootScope.instructor = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toFdyOneStudent = function(){
        location.href = '#/index/instructor/fdyGrantSHQOneStudent'
    }
    $scope.toFdyGrantRegister = function(){
        // location.href = '#/index/instructor/';
    }
    $scope.toFdyZHNSHScholarship = function(){
        location.href = '#/index/instructor/fdyZHNSHScholarship';
    }
    $scope.toFdyGZHScholarship = function(){
        location.href = '#/index/instructor/fdyGZHScholarship';
    }
}