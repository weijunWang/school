angular
    .module('inspinia')
    .controller('SrmCheckProNoCtrl', SrmCheckProNoCtrl)
function SrmCheckProNoCtrl($scope, $rootScope) {
    console.log('SrmCheckProNoCtrl');
    $rootScope.srmStaffNav = true;
    $scope.backProApp = function () {
        location.href = '#/index/scientificResearchMana/srmProApplica';
    }
    $scope.allLis = [
        {
            name: '学科系',
            status: '已审核',
            time: '2017.10.27'
        },{
            name: '科研管理科',
            status: '已审核',
            time: '2017.10.27'
        },{
            name: '学院院长',
            status: '暂未审核',
            time: ''
        }
    ]
    $scope.changePro = function(){
        location.href = '#/index/scientificResearchMana/srmChangePro';
    }
}