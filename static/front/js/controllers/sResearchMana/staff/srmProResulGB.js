angular
    .module('inspinia')
    .controller('SrmProResulGBCtrl', SrmProResulGBCtrl)
function SrmProResulGBCtrl($scope, $rootScope) {
    console.log('SrmProResulGBCtrl');
    $rootScope.srmStaffNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        }, {
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $scope.newPro = function () {
        location.href = '#/index/scientificResearchMana/srmProResulGBCh/1';
    }
    $scope.toCheckPro = function () {
        if (this.$index == 0) {
            location.href = '#/index/scientificResearchMana/srmProResulGBCh/2';
        } else {
            location.href = '#/index/scientificResearchMana/srmProResulGBCh/3';
        }
    }
    // 论文
    $scope.toL = function () {
        location.href = '#/index/scientificResearchMana/srmProResul'
    }
    // 获奖
    $scope.toH = function () {
        location.href = '#/index/scientificResearchMana/srmProResulHJ'
    }
    // 专著
    $scope.toZHZH = function () {
        location.href = '#/index/scientificResearchMana/srmProResulZHZHU'
    }
    //专利
    $scope.toZHL = function () {
        location.href = '#/index/scientificResearchMana/srmProResulZHL'
    }
    //咨询报告
    $scope.toZX = function () {
        location.href = '#/index/scientificResearchMana/srmProResulZX'
    }
    // 成果转化
    $scope.toCHG = function () {
        location.href = '#/index/scientificResearchMana/srmProResulCHG'
    }
    // 国内外学术会议报告
    $scope.toGN = function () {
        location.href = '#/index/scientificResearchMana/srmProResulGB'
    }
    // 国内外参加学术会议
    $scope.toGW = function () {
        location.href = '#/index/scientificResearchMana/srmProResulGC'
    }
    // 国外进修（合作研究）
    $scope.toGWJ = function () {
        location.href = '#/index/scientificResearchMana/srmProResulGWJ'
    }
}