angular
    .module('inspinia')
    .controller('SrmProGetCheckCtrl', SrmProGetCheckCtrl)
function SrmProGetCheckCtrl($scope, $rootScope, $stateParams) {
    console.log('SrmProGetCheckCtrl');
    console.log('proId', $stateParams.proId);
    $rootScope.srmStaffNav = true;
    $scope.proId = Number($stateParams.proId)
    switch($scope.proId){
        case 1:
            $scope.proTitle = '新建纵向科研项目获得'
            $scope.topArr = [
                {
                    text: '新建纵向科研项目获得'
                }
            ];
            break;
        case 2:
            $scope.proTitle = '项目获得详情';
            $scope.topArr = [
                {
                    text: '纵向科研项目获得'
                }
            ]
            break;
        case 3:
            $scope.proTitle = '项目获得详情';
            $scope.topArr = [
                {
                    text: '纵向科研项目获得'
                }
            ];
            break;
        case 4:
            $scope.proTitle = '修改项目获得详情';
            $scope.topArr = [
                {
                    text: '纵向科研项目获得'
                },{
                    text: '修改纵向科研项目获得'
                }
            ]
            break;
    }
    
    $scope.backProApp = function () {
        location.href = '#/index/scientificResearchMana/srmProGet';
    }
    $scope.isBack = function(){
        console.log(this.$index);
        if(this.$index == 1){
            location.href = '#/index/scientificResearchMana/srmProGetCheck/3';
        }else{
            location.href = '#/index/scientificResearchMana/srmProGet';
        }
    }
    $scope.allLis = [
        {
            name: '学科系',
            status: '已审核',
            time: '2017.10.27'
        }, {
            name: '科研管理科',
            status: '已审核',
            time: '2017.10.27'
        }, {
            name: '学院院长',
            status: '暂未审核',
            time: ''
        }
    ]
    $scope.changePro = function () {
        location.href = '#/index/scientificResearchMana/srmProGetCheck/4';
    }
    $scope.toH = function(){
        location.href = '#/index/scientificResearchMana/srmProGetH'
    }
}