angular
    .module('inspinia')
    .controller('SrmProGetHCtrl', SrmProGetHCtrl)
function SrmProGetHCtrl($scope, $rootScope) {
    console.log('SrmProGetHCtrl');
    $rootScope.srmStaffNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        }, {
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $scope.newPro = function () {
        location.href = '#/index/scientificResearchMana/srmProGetCheckH/1';
    }
    $scope.toCheckPro = function () {
        if (this.$index == 0) {
            location.href = '#/index/scientificResearchMana/srmProGetCheckH/2';
        } else {
            location.href = '#/index/scientificResearchMana/srmProGetCheckH/3';
        }
    }
    $scope.toZ = function(){
        location.href = '#/index/scientificResearchMana/srmProGet'
    }
}