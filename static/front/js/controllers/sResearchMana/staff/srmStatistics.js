angular
    .module('inspinia')
    .controller('SrmStatisticsCtrl', SrmStatisticsCtrl)
function SrmStatisticsCtrl($scope, $rootScope) {
    console.log('SrmStatisticsCtrl');
    $rootScope.srmStaffNav = true;
    $scope.allNews = [
        {
            text: '学校最新政策通知2017年2月奖学金评选政策',
            time: '2017年1月3日'
        }, {
            text: '学校最新政策通知2017年2月奖学金评选政策',
            time: '2017年1月3日'
        }
    ]
    // 折线图
    var dom1 = document.getElementById("container1");
    var myChart1 = echarts.init(dom1);
    var app1 = {};
    option1 = null;
    option1 = {
        xAxis: {
            type: 'category',
            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            data: [820, 932, 901, 934, 1290, 1330, 1320],
            type: 'line'
        }]
    };
    if (option1 && typeof option1 === "object") {
        myChart1.setOption(option1, true);
    }

    var dom2 = document.getElementById("container2");
    var myChart2 = echarts.init(dom2);
    var app2 = {};
    option2 = null;
    app2.title = '坐标轴刻度与标签对齐';

    option2 = {
        color: ['#3398DB'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                axisTick: {
                    alignWithLabel: true
                }
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '直接访问',
                type: 'bar',
                barWidth: '60%',
                data: [10, 52, 200, 334, 390, 330, 220]
            }
        ]
    };
    if (option2 && typeof option2 === "object") {
        myChart2.setOption(option2, true);
    }

    var dom3 = document.getElementById("container3");
    var myChart3 = echarts.init(dom3);
    var app3 = {};
    option3 = null;
    option3 = {
        title: {
            text: '12',
            subtext: '纯属虚构',
            x: 'center'
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: ['科研1', '科研2', '科研3', '科研4', '科研5']
        },
        series: [
            {
                name: '科研',
                type: 'pie',
                radius: '55%',
                center: ['50%', '60%'],
                data: [
                    { value: 335, name: '科研1' },
                    { value: 310, name: '科研2' },
                    { value: 234, name: '科研3' },
                    { value: 135, name: '科研4' },
                    { value: 1548, name: '科研5' }
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };
    if (option3 && typeof option3 === "object") {
        myChart3.setOption(option3, true);
    }
}