angular
    .module('inspinia')
    .controller('SrmProResulCHGChCtrl', SrmProResulCHGChCtrl)
function SrmProResulCHGChCtrl($scope, $rootScope, $stateParams) {
    console.log('SrmProResulCHGChCtrl');
    console.log('proId', $stateParams.proId);
    $rootScope.srmStaffNav = true;
    $scope.proId = Number($stateParams.proId)
    switch ($scope.proId) {
        case 1:
            $scope.proTitle = '新建纵向科研项目获得'
            $scope.topArr = [
                {
                    text: '新建纵向科研项目获得'
                }
            ];
            break;
        case 2:
            $scope.proTitle = '项目获得详情';
            $scope.topArr = [
                {
                    text: '纵向科研项目获得'
                }
            ]
            break;
        case 3:
            $scope.proTitle = '项目获得详情';
            $scope.topArr = [
                {
                    text: '纵向科研项目获得'
                }
            ];
            break;
        case 4:
            $scope.proTitle = '修改项目获得详情';
            $scope.topArr = [
                {
                    text: '纵向科研项目获得'
                }, {
                    text: '修改纵向科研项目获得'
                }
            ]
            break;
    }

    $scope.backProApp = function () {
        location.href = '#/index/scientificResearchMana/srmProResulCHG';
    }
    $scope.isBack = function () {
        console.log(this.$index);
        if (this.$index == 1) {
            location.href = '#/index/scientificResearchMana/srmProResulCHGCh/3';
        } else {
            location.href = '#/index/scientificResearchMana/srmProResulCHG';
        }
    }
    $scope.allLis = [
        {
            name: '学科系',
            status: '已审核',
            time: '2017.10.27'
        }, {
            name: '科研管理科',
            status: '已审核',
            time: '2017.10.27'
        }, {
            name: '学院院长',
            status: '暂未审核',
            time: ''
        }
    ]
    $scope.changePro = function () {
        location.href = '#/index/scientificResearchMana/srmProResulCHGCh/4';
    }
    // 论文
    $scope.toL = function () {
        location.href = '#/index/scientificResearchMana/srmProResul'
    }
    // 获奖
    $scope.toH = function () {
        location.href = '#/index/scientificResearchMana/srmProResulHJ'
    }
    // 专著
    $scope.toZHZH = function () {
        location.href = '#/index/scientificResearchMana/srmProResulZHZHU'
    }
    //专利
    $scope.toZHL = function () {
        location.href = '#/index/scientificResearchMana/srmProResulZHL'
    }
    //咨询报告
    $scope.toZX = function () {
        location.href = '#/index/scientificResearchMana/srmProResulZX'
    }
    // 成果转化
    $scope.toCHG = function () {
        location.href = '#/index/scientificResearchMana/srmProResulCHG'
    }
    // 国内外学术会议报告
    $scope.toGN = function () {
        location.href = '#/index/scientificResearchMana/srmProResulGB'
    }
    // 国内外参加学术会议
    $scope.toGW = function () {
        location.href = '#/index/scientificResearchMana/srmProResulGC'
    }
    // 国外进修（合作研究）
    $scope.toGWJ = function () {
        location.href = '#/index/scientificResearchMana/srmProResulGWJ'
    }
}