angular
.module('inspinia')
.controller('StudentDepartmentCtrl', StudentDepartmentCtrl)
function StudentDepartmentCtrl($scope,$rootScope, IndexInfo){
    console.log('StudentDepartmentCtrl');
    $rootScope.department = true;
    $scope.indexInfo = IndexInfo;
    $scope.changeHidden = function(){
        console.log(this.$index);
        for(var i = 0;i < $scope.indexInfo.length;i++){
            $scope.indexInfo[i].isHidden = false;
        }
        $scope.indexInfo[this.$index].isHidden = true;
    }
}