angular
.module('inspinia')
.controller('SdGrantCtrl', SdGrantCtrl)
function SdGrantCtrl($scope,$rootScope, IndexInfo){
    console.log('SdGrantCtrl');
    $rootScope.department = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toOneStudent = function(){
        location.href = '#/index/studentDepartment/sdGrantRegister'
    }
    $scope.toGrantRegister = function(){
        location.href = '#/index/studentDepartment/sdGrantSHQForm';
    }
    $scope.toZHNSHScholarship = function(){
        location.href = '#/index/studentDepartment/sdZHNSHScholarship';
    }
    $scope.toGrantCheck = function(){
        location.href = '#/index/studentDepartment/sdGrantCheck';
    }
}