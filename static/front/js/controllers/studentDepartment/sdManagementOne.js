angular
.module('inspinia')
.controller('SdManagementOneCtrl', SdManagementOneCtrl)
function SdManagementOneCtrl($scope,$rootScope, IndexInfo){
    console.log('SdManagementOneCtrl');
    $rootScope.department = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toOneStudent = function(){
        location.href = '#/index/studentDepartment/sdManaOneStudent'
    }
    $scope.toSdInstructor = function(){
        location.href = '#/index/studentDepartment/sdInstructor';
    }
    $scope.toFdyOtherHonor = function(){
        location.href = '#/index/instructor/fdyOtherHonor';
    }
}