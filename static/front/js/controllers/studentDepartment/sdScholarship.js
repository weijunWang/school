angular
.module('inspinia')
.controller('SdScholarshipCtrl', SdScholarshipCtrl)
function SdScholarshipCtrl($scope,$rootScope, IndexInfo){
    console.log('SdScholarshipCtrl');
    $rootScope.department = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toOneStudent = function(){
        location.href = '#/index/studentDepartment/sdScholarshipOne'
    }
    $scope.toGJLZHScholarship = function(){
        location.href = '#/index/studentDepartment/sdGJScholarship';
    }
    $scope.toZHNSHScholarship = function(){
        location.href = '#/index/studentDepartment/sdZHScholarship';
    }
    $scope.toGZHScholarship = function(){
        location.href = '#/index/studentDepartment/sdGZHScholarship';
    }
}