angular
.module('inspinia')
.controller('SdInstructorCtrl', SdInstructorCtrl)
function SdInstructorCtrl($scope,$rootScope, IndexInfo){
    console.log('SdInstructorCtrl');
    $rootScope.department = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toSdOne = function(){
        location.href = '#/index/studentDepartment/sdInstructorOne';
    }
}