
angular
    .module('inspinia')
    .controller('TeNoticeCtrl', TeNoticeCtrl)
function TeNoticeCtrl($scope, $rootScope) {
    console.log('TeNoticeCtrl');
    $scope.student = false;
    $scope.instructor = false;
    $scope.office = false;
    $rootScope.srmStaffNav = false;
    $rootScope.teachingNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        },{
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $(function () {
        $('.dept_select').chosen();
        $('.dept_select').change(function () {
            console.log(this.value);
        });
    });
    $scope.toNoticeInfo = function(){
        if(this.$index == 0){
            location.href = '#/index/teaching/teNoticeInfo';
        }else{
            var proId = 2;
            location.href = '#/index/teaching/teNoticeInfoOne/' + proId;
        }
        
    }
    $scope.newPro = function(){
        var proId = 1;
        location.href = '#/index/teaching/teNoticeInfoOne/' + proId;
    }
}