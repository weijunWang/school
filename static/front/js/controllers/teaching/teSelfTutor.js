angular
    .module('inspinia')
    .controller('TeSelfTutorCtrl', TeSelfTutorCtrl)
function TeSelfTutorCtrl($scope, $rootScope) {
    console.log('TeSelfTutorCtrl');
    $scope.student = false;
    $scope.instructor = false;
    $scope.office = false;
    $rootScope.srmStaffNav = false;
    $rootScope.teachingNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        }, {
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $scope.proTitle = '查看大学生创新创业项目';
    $scope.newPro = function () {
        location.href = '#/index/teaching/tePracManaCh/1';
    }
    $scope.toCheckPro = function () {
        if (this.$index == 0) {
            location.href = '#/index/teaching/tePracManaCh/2';
        } else {
            location.href = '#/index/teaching/tePracManaCh/3';
        }
    }
    $scope.toD = function () {
        location.href = '#/index/teaching/teSelfTutor';
    }
    $scope.toP = function () {
        location.href = '#/index/teaching/teSelfEval';
    }
}