angular
    .module('inspinia')
    .controller('TeGraMaTitleCtrl', TeGraMaTitleCtrl)
function TeGraMaTitleCtrl($scope, $rootScope) {
    console.log('TeGraMaTitleCtrl');
    $scope.student = false;
    $scope.instructor = false;
    $scope.office = false;
    $rootScope.srmStaffNav = false;
    $rootScope.teachingNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        }, {
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $scope.newPro = function () {
        location.href = '#/index/teaching/teGraMaTitleCh/1';
    }
    $scope.toCheckPro = function () {
        if (this.$index == 0) {
            location.href = '#/index/teaching/teGraMaTitleCh/2';
        } else {
            location.href = '#/index/teaching/teGraMaTitleCh/3';
        }
    }
    // 开题信息
    $scope.toTeAc = function(){
        location.href = '#/index/teaching/teGraMaTitle'
    }
    // 答辩管理-答辩小组
    $scope.toR = function(){
        location.href = '#/index/teaching/teDefManTeam'
    } 
    // 答辩管理-答辩成绩
    $scope.toA = function(){
        location.href = '#/index/teaching/teDefAchiev'
    }
    // 毕业成绩
    $scope.toL = function () {
        location.href = '#/index/teaching/teGraduAchiev'
    }
}