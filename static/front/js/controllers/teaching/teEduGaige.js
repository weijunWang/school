angular
    .module('inspinia')
    .controller('TeEduGaigeCtrl', TeEduGaigeCtrl)
function TeEduGaigeCtrl($scope, $rootScope) {
    console.log('TeEduGaigeCtrl');
    $scope.student = false;
    $scope.instructor = false;
    $scope.office = false;
    $rootScope.srmStaffNav = false;
    $rootScope.teachingNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        }, {
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $scope.newPro = function () {
        location.href = '#/index/teaching/teEduGaigeCh/1';
    }
    $scope.toCheckPro = function () {
        if (this.$index == 0) {
            location.href = '#/index/teaching/teEduGaigeCh/2';
        } else {
            location.href = '#/index/teaching/teEduGaigeCh/3';
        }
    }
    $scope.toTeAc = function(){
        location.href = '#/index/teaching/teAchievStuPro'
    }
    // 教学研究论文
    $scope.toR = function(){
        location.href = '#/index/teaching/teResearchPaper'
    }
    // 教学改革项目
    $scope.toL = function () {
        location.href = '#/index/teaching/teEduGaige'
    }
    // 教材建设
    $scope.toH = function () {
        location.href = '#/index/teaching/teMetCons'
    }
    // 教学获奖
    $scope.toZHZH = function () {
        location.href = '#/index/teaching/teTeWin'
    }
}