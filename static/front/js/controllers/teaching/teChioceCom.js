angular
    .module('inspinia')
    .controller('TeChioceComCtrl', TeChioceComCtrl)
function TeChioceComCtrl($scope, $rootScope) {
    console.log('TeChioceComCtrl');
    $scope.student = false;
    $scope.instructor = false;
    $scope.office = false;
    $rootScope.srmStaffNav = false;
    $rootScope.teachingNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        }, {
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $scope.choiceOneCom = function(){
        $('#myModal').modal('show');
    }
    $scope.quitOneCom = function(){
        $('#myModal2').modal('show');
    }
    $scope.toR = function(){
        location.href = '#/index/teaching/teInComPaperGui';
    }
}