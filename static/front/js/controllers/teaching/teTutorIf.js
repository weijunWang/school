angular
    .module('inspinia')
    .controller('TeTutorIfCtrl', TeTutorIfCtrl)
function TeTutorIfCtrl($scope, $rootScope) {
    console.log('TeTutorIfCtrl');
    $scope.student = false;
    $scope.instructor = false;
    $scope.office = false;
    $rootScope.srmStaffNav = false;
    $rootScope.teachingNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '不通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '通过'
        }, {
            time: '2018.02.09—2018.03.09',
            name: '通过',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '不通过'
        }
    ]
    $scope.newPro = function () {
        location.href = '#/index/teaching/teTutorIfCh/1';
    }
    $scope.toCheckPro = function () {
        if (this.$index == 0) {
            location.href = '#/index/teaching/teTutorIfCh/2';
        } else {
            location.href = '#/index/teaching/teTutorIfCh/3';
        }
    }
    // 导师信息
    $scope.toTeAc = function(){
        location.href = '#/index/teaching/teTutorIf';
    }
    // 学生信息
    $scope.toR = function(){
        location.href = '#/index/teaching/teTutorStuIf';
    }
}