
angular
    .module('inspinia')
    .controller('TeachingCtrl', TeachingCtrl)
function TeachingCtrl($scope, $rootScope) {
    console.log('TeachingCtrl');
    $scope.student = false;
    $scope.instructor = false;
    $scope.office = false;
    $rootScope.srmStaffNav = false;
    $rootScope.teachingNav = true;

    $(function(){
        $('.dept_select').chosen({
            // disable_search: false,
            no_results_text: '你是的内容不存在，请重新输入!',//输入内容不存在时
            // placeholder_text_single: '123123'
            search_contains: true,//默认false是从option第一个字开始匹配的，true为所有的都进行匹配
            // width: '95%'
            // inherit_select_classes: true
        });
        $('.dept_select').change(function(){
            console.log(this.value);
        });
    });
}