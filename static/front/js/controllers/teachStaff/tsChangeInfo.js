angular
.module('inspinia')
.controller('TsChangeInfoCtrl', TsChangeInfoCtrl)
function TsChangeInfoCtrl($scope,$rootScope, IndexInfo, myFactory){
    console.log('TsChangeInfoCtrl');
    $rootScope.teachStaffNav = true;
    $scope.indexInfo = IndexInfo;
    $scope.infoUrl = '';
    console.log(myFactory.getStorage('selfInfoNum'));
    switch(Number(myFactory.getStorage('selfInfoNum'))){
        case 0:
            $scope.infoUrl = '../../static/front/views/common/changeInfo.html';
            break;
        case 1:
            $scope.infoUrl = '../../static/front/views/common/education.html';
            break;
        case 2:
            $scope.infoUrl = '../../static/front/views/common/continuingEducation.html';
            break;
        case 3:
            $scope.infoUrl = '../../static/front/views/common/training.html';
            break;
        case 4:
            $scope.infoUrl = '../../static/front/views/common/academicConf.html';
            break;
        case 5:
            $scope.infoUrl = '../../static/front/views/common/study.html';
            break;
        case 6:
            $scope.infoUrl = '../../static/front/views/common/partTimeJob.html';
            break;
        case 7:
            $scope.infoUrl = '../../static/front/views/common/socialService.html';
            break;
        default: 
            break;
    }
    $scope.checkInfo = function(){
        console.log('checkInfo');
    }
    $scope.addInfo = function(){
        console.log('addInfo');
        // location.href = '#/index/teachStaff/tsAddInfo'
    }
    $scope.changeHidden = function(){
        console.log(this.$index);
        for(var i = 0;i < $scope.indexInfo.length;i++){
            $scope.indexInfo[i].isHidden = false;
        }
        $scope.indexInfo[this.$index].isHidden = true;
    }
}