angular
.module('inspinia')
.controller('TeachStaffCtrl', TeachStaffCtrl)
function TeachStaffCtrl($scope,$rootScope, IndexInfo){
    console.log('TeachStaffCtrl');
    $rootScope.teachStaffNav = true;
    $scope.indexInfo = IndexInfo;
    $scope.changeHidden = function(){
        console.log(this.$index);
        for(var i = 0;i < $scope.indexInfo.length;i++){
            $scope.indexInfo[i].isHidden = false;
        }
        $scope.indexInfo[this.$index].isHidden = true;
    }
}