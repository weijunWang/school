angular
.module('inspinia')
.controller('TsSelfInfoCtrl', TsSelfInfoCtrl)
function TsSelfInfoCtrl($scope,$rootScope, IndexInfo, myFactory){
    console.log('TsSelfInfoCtrl');
    $rootScope.teachStaffNav = true;
    $scope.indexInfo = IndexInfo;
    for(var i = 0;i < $scope.indexInfo.length;i++){
        $scope.indexInfo[i].content = [];
    }
    $scope.indexInfo[0].content = [
        {key: '科室',value: '1103'},
        {key: '工号',value: 'cxb245'},
        {key: '姓名',value: '张传明'},
        {key: '性别',value: '男'},
        {key: '民族',value: '汉'},
        {key: '出生年月',value: '19701020'},
        {key: '年龄',value: '38'},
        {key: '婚姻状况',value: '已婚'},
        {key: '身份证',value: '172190197010208907'},
        {key: '家庭详细地址',value: '贵州省贵阳市开阳区xxxxx'},
        {key: '入校时间',value: '2001年5月'},
        {key: '政治面貌',value: '党员'},
        {key: '任职状态',value: '在职'},
        {key: '学历',value: '博士研究生'},
        {key: '学位',value: '博士'},
        {key: '毕业院校',value: '贵州医科大学'},
        {key: '专业',value: '临床医学'},
        {key: '职务',value: '职务'},
        {key: '导师类别',value: '博士导师'},
        {key: '手机',value: '18956894563'},
        {key: 'QQ',value: '698745612'},
        {key: '邮箱',value: 'zhangchuanming@163.com'}
        // {house: '1103'},
        // {number: 'cxb245'},
        // {name: '张传明'},
        // {sex: '男'},
        // {minzu: '汉'},
        // {birthday: '19701020'},
        // {age: '38'},
        // {nativePlace: '贵州省贵阳市'},
        // {cardID: '172190197010208907'},
        // {detailedHomeAddress: '贵州省贵阳市开阳区xxxxx'},
        // {schoolTime: '2001年5月'},
        // {politicalOutlook: '党员'},
        // {stateOfService: '在职'},
        // {education: '博士研究生'},
        // {academicDegree: '博士'},
        // {universityOneIsGraduatedFrom: '贵州医科大学'},
        // {academicMargin: '本校'},
        // {categoryOfPersonnel: '专业技术人员'},
        // {professionalTechnicalTitle: '专业技术人员'},
        // {post: '职务'},
        // {classOfTutors: '博士导师'},
        // {phone: '18956894563'},
        // {QQ: '678793'},
        // {email: 'zhangchuanming@163.com'}
    ]
    for(var i = 1;i < $scope.indexInfo.length; i++){
        $scope.indexInfo[i].content = [
            {
                data: '2011.09—2015.07',
                school: '贵州医科大学',
                academicDegree: '博士',
                sponsor: '赞助',
                cultureDescription: '培养描述',
                attachments: '附件描述'
            },{
                data: '2011.09—2015.07',
                school: '贵州医科大学',
                academicDegree: '博士',
                sponsor: '赞助',
                cultureDescription: '培养描述',
                attachments: '附件描述'
            }
        ]
    }
    $scope.checkInfo = function(){
        console.log('checkInfo');
        changeHidden(this.$index)
        myFactory.setStorage('selfInfoNum', this.$index);
        location.href = '#/index/teachStaff/tsChangeInfo';
    }
    function changeHidden(index){
        console.log(index);
        for(var i = 0;i < $scope.indexInfo.length;i++){
            $scope.indexInfo[i].isHidden = false;
        }
        $scope.indexInfo[index].isHidden = true;
    }
    $scope.changeHidden = function(){
        changeHidden(this.$index)
    }
}