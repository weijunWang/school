angular
.module('inspinia')
.controller('PtAfManNoticeCtrl', PtAfManNoticeCtrl)
function PtAfManNoticeCtrl($scope,$rootScope, IndexInfo, $http){
    console.log('PtAfManNoticeCtrl');
    $rootScope.partyAffManaNav = true;
    $scope.allInfos = [
        {
            time: '2018.02.09—2018.03.09',
            name: '董传鹏',
            class: '临床一班',
            type: '事假',
            reason: '具体事件原因',
            file: '附件',
            status: '已通过'
        }
    ]
    $scope.toCheckPro = function(){
        // location.href = '#/index/partyAffMana/ptAfManNotiOne'
        // console.log(12);
        // $http({
        //     method: "GET",
        //     url: 'http://192.168.15.46:8000/getRes/',
        //     withCredentials: true,
        // }).then(function(data){
        //     console.log(data)
        // }, function(err){
        //     console.log('err', err);
        // });
        $http({
            method: "POST",
            url: 'http://www.tuling123.com/openapi/api',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            // withCredentials: true,
            data : {
                key: '0a1a9157e80341738da6de00276bae75',
                info: '天气',
                loc: '上海',
                userid: '123'
            },
            // transformRequest: function (data, headersGetter) {
            //     var formData = new FormData();
            //     angular.forEach(data, function (value, key) {
            //         formData.append(key, value);
            //     });

            //     var headers = headersGetter();
            //     delete headers['Content-Type'];

            //     return formData;
            // }
        }).then(function(response){
            console.log(response);
        },function(response){
            console.log("err",response);
        });
    }
    $scope.toNewPro = function(){
        location.href = '#/index/partyAffMana/ptAfManNewNoti';
    }
}