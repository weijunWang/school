angular
.module('inspinia')
.controller('PtAfManSelfInfoCtrl', PtAfManSelfInfoCtrl)
function PtAfManSelfInfoCtrl($scope,$rootScope, IndexInfo){
    console.log('PtAfManSelfInfoCtrl');
    $rootScope.partyAffManaNav = true;
    $scope.indexInfo = IndexInfo;
    $scope.changeHidden = function(){
        console.log(this.$index);
        for(var i = 0;i < $scope.indexInfo.length;i++){
            $scope.indexInfo[i].isHidden = false;
        }
        $scope.indexInfo[this.$index].isHidden = true;
    }
}